import { MiddlewareConsumer, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RequestContextModule } from './request-context/request-context.module';
import { CustomLoggerModule } from './custom-logger/custom-logger.module';
import { RequestContextMiddleware } from './request-context/request-context.middleware';

@Module({
  imports: [RequestContextModule, CustomLoggerModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer){
    consumer.apply(RequestContextMiddleware).forRoutes('*');
  }
}
