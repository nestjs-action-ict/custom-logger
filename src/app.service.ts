import { Injectable } from '@nestjs/common';
import { RequestContextService } from './request-context/request-context.service';
import { CustomLogger } from './custom-logger/custom-logger';

@Injectable()
export class AppService {

  private readonly logger: CustomLogger

  constructor( private readonly requestContextService: RequestContextService) {
    this.logger = new CustomLogger(requestContextService, AppService.name)
  }

  getHello(): string {
    this.logger.log("I am in the service")
    return 'Hello World!';
  }
}
