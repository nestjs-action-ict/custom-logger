import { Injectable } from '@nestjs/common';
import { RequestContext } from './request-context.model';

@Injectable()
export class RequestContextService {
  get currentRequestId(): number | null {
    const requestContext = this.currentRequest;
    return (requestContext && requestContext.requestId) || null;
  }

  get currentContext(): any | null {
    const requestContext = this.currentRequest;
    return (requestContext && requestContext.context) || null;
  }

  get currentRequest(): any | null {
    const requestContext = RequestContext.currentContext;
    return requestContext || null;
  }


}

