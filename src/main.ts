import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { WinstonModule } from 'nest-winston';
import { format, transports } from 'winston';



async function bootstrap() {
  const app = await NestFactory.create(AppModule,
    {
      logger: WinstonModule.createLogger({
        exitOnError: false,
        format: format.combine(
          format.colorize(),
          format.timestamp(),
          format.printf(({level, message, timestamp, ...meta}) => {
            if (meta.context && meta.context.id)  {
              return `${timestamp} [${meta.context.id}] ${level} ${meta.context.name} [${meta.context.pid || ''};${meta.context.cid || ''};${meta.context.sid || ''};${meta.context.rid || ''};${meta.context.qid || ''}] - ${message}`;
            }
            return `${timestamp} ${level} - ${message}`;
        })),
        transports: [new transports.Console({ level: "debug" })], // alert > error > warning > notice > info > debug
      }),
    });
  //app.useLogger(app.get(CustomLogger));
  await app.listen(3000);
}
bootstrap();
